# - `docker build -f Dockerfile -t ens-lua:latest .`
# - `docker run -it -v $(pwd):/root/ens ens-lua:latest`
# - `cd ens`
# - `lua resolve-ens.lua`

FROM debian:bookworm

RUN apt-get update
RUN apt-get install --yes curl dnsdist dnsutils git liblua5.1-dev lua5.1 luajit libssh-dev make zip

WORKDIR /root
RUN git clone --progress https://github.com/luarocks/luarocks.git
WORKDIR /root/luarocks
RUN git checkout v3.9.2
RUN ./configure
RUN make
RUN make install
WORKDIR /root

RUN luarocks install luasec
RUN luarocks install dkjson
RUN luarocks install luasocket

#RUN curl --no-progress-meter --output /usr/local/share/lua/5.1/sha2.lua https://raw.githubusercontent.com/Egor-Skriptunoff/pure_lua_SHA/master/sha2.lua
COPY sha2-for-eth.lua /usr/local/share/lua/5.1/sha2.lua

WORKDIR /root
RUN echo "alias l='ls -l'" >> .bashrc
