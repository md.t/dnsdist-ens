-- resolve-ens.lua

-- Packages

local json = require 'dkjson'
local https = require 'ssl.https'
local ltn12 = require 'ltn12'

-- pure_lua_SHA
-- https://github.com/Egor-Skriptunoff/pure_lua_SHA/
local sha = require 'sha2'
local keccak = sha.sha3_256
local hex2bin = sha.hex_to_bin


-- Globals

g_verbose = false


-- Common Functions

-- print `o` on one line
local function dump (o)
    if type(o) == 'table' then
        local s = '{ '
        for k,v in pairs(o) do
            if type(k) ~= 'number' then k = '"'..k..'"' end
            s = s..'['..k..'] = '..dump(v)..','
        end
        return s..' }'
    else
        return tostring(o)
    end
end


local function error (str)
    io.stderr:write('ERROR: ' .. str .. '\n')
end


-- print `obj` on multiple lines
local function printObj (obj, hierarchyLevel)
    if (hierarchyLevel == nil) then
        hierarchyLevel = 0
    elseif (hierarchyLevel == 4) then
        return 0
    end
    local whitespace = ""
    for i=0,hierarchyLevel,1 do
        whitespace = whitespace.."|"
    end
    io.write(whitespace)
    print(obj)
    if (type(obj) == "table") then
        for k,v in pairs(obj) do
            io.write(whitespace.."  ")
            if (type(v) == "table") then
                printObj(v, hierarchyLevel+1)
            else
                print(k ..": ".. v)
            end
        end
    else
        print(obj)
    end
end


local function verbose (str)
    if g_verbose then
        print(str)
    end
end



-- From web3.lua: https://github.com/MrSyabro/web3.lua

local function tohex (str)
    if type(str) == "string" then
        return (str:gsub('.', function (c)
            return string.format('%02x', string.byte(c))
        end))
    elseif type(str) == "number" then
        return string.format("%x", str)
    end
end


local function fromhex (str)
    return (str:gsub('..', function (cc)
        return string.char(tonumber(cc, 16))
    end))
end


local function expand_left (str)
    local s = 64 - string.len(str)
    str = string.rep("0", s) .. str
    return str
end

local function expand (str)
    return expand_left(str)
end


local function expand_right (str)
    local s = 64 - string.len(str)
    str = str .. string.rep("0", s)
    return str
end


-- Functions

local function json_rpc_call (method, params)
    --https.TIMEOUT = 2
    verbose('    • [json_rpc_call(' .. method .. ', <params>)]')
    local json_rpc = json.encode(
        { jsonrpc = '2.0', method = method, params = params,
          id = math.floor(math.random() * 2147483647) })
    verbose('      • json_rpc: ' .. dump(json_rpc))
    local response = {}
    local code, status = https.request(
        { url = 'https://eth-rpc.gateway.pokt.network/',
          sink = ltn12.sink.table(response),
          source = ltn12.source.string(json_rpc),
          method = 'POST',
          headers = { ['content-length'] = #json_rpc,
                      ['content-type'] = 'application/json' }
    })
    verbose('      • status: ' .. status)
    return code, status, response
end


-- https://eips.ethereum.org/EIPS/eip-137
local function namehash (name)
    local labels = {}
    local node = string.rep('0', 64)
    for w in name:gmatch('([^.]+)') do table.insert(labels, w) end
    for i = #labels, 1, -1 do
        -- Gaaaah, I don't understand why the outer `keccak` needs `hex2bin`
        -- input but the inner does not!
        -- Something to do with the concatenation, but does `keccak` detect
        -- if it gets bin or hex input?
        node = keccak(hex2bin(node) .. hex2bin(keccak(labels[i])))
    end
    return node
end


local function ens_resolver_call (data)
    verbose('  • [ens_resolver_call(<data>)]')
    local ens_resolver = '0x4976fb03C32e5B8cfe2b6cCB31c09Ba78EBaBa41'
    local params = {{ ['to'] = ens_resolver, ['data'] = data }, 'latest' }
    verbose('    • params: ' .. dump(params))
    return json_rpc_call('eth_call', params)
end


-- This is a call to the `addr(bytes32)` function of the default ENS resolver
-- contract (0x4976fb) with as argument `foo.eth`.
-- It returns the wallet address associated with `foo.eth`: 0xce1e62 padded
-- to 32 bytes.
local function ens_addr (name)
    verbose('• [ens_addr(' .. name .. ')]')
    -- `3b3b57de` <- `string.sub(keccak('addr(bytes32)'),1,8)`
    local data = '0x3b3b57de' .. (namehash(name))
    verbose('  • data: ' .. data)
    local code, status, response = ens_resolver_call(data)
    if (status == 200) then
        return '0x' .. string.sub(json.decode(response[1])['result'], 27)
    else
        return nil
    end
end


-- `text(bytes32,string)` returns a string, encoded as follows:
-- - bytes 00-31: offset to dynamic data
-- - bytes 32-63: length of dynamic data
-- - bytes 64-  : actual string
local function ens_text (name, field)
    verbose('• [ens_text(' .. name .. ',' .. field .. ')]')
    -- `59d1d43c` <- `string.sub(keccak('text(bytes32,string)'),1,8)`
    local data = '0x59d1d43c' .. namehash(name) ..
                 expand_left(tohex(64))     ..  -- offset
                 expand_left(tohex(#field)) ..  -- length
                 expand_right(tohex(field))
    verbose('  • data: ' .. data)
    local code, status, response = ens_resolver_call(data)
    verbose('  • response: ' .. table.concat(response))
    if (status == 200) then
        -- strip off "0x"
        local result = string.sub(json.decode(response[1])['result'], 3)
        -- The lengths are all times 2 since we're working on the hex string.
        local length = tonumber(string.sub(result, 65, 128), 16)
        -- `length * 2` because (again) we're working on the hex string
        -- not sure why I need the `-1`, Lua I guess...
        local content = fromhex(string.sub(result, 129, 129 + length * 2 - 1))
        return content
    else
        return nil
    end
end


-- DNSdist Functions

local function resolve_ens (dq)
    local qname = tostring(dq.qname)
    local name = string.sub(qname, 0, string.len(qname) - 1)
    print('• request for *.eth domain: ' .. name)
    local ens_name = ens_text(name, 'url')
    -- XXX add "if starts with http" check
    local words = {}
    for w in ens_name:gmatch('([^/]+)') do table.insert(words, w) end
    if words[2] then
        print("  • host found in 'url' field: " .. words[2])
        return DNSAction.Spoof, words[2]
    else
        print('  • nothing useful found, returning NXDOMAIN')
        return DNSAction.Nxdomain
    end
end


-- Main Program

math.randomseed(os.time())

print('ENS functions loaded.')


-- Package Stuff

local ens = {
    resolve_ens = resolve_ens
}

return ens
